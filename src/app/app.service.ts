import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

// RXJS
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class AppService {
  private steamidUrl = '/api/user';  // URL to web api
  private logoutUrl = '/api/logout';  // URL to web api

  constructor(private http: Http) {  }

  private extractData(res: Response) {
    console.log(res)
    const body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  getUser(): Observable<any> {
    return this.http.get(this.steamidUrl).map(this.extractData).catch(this.handleError);
  }

  closeSession() {
    return this.http.get(this.logoutUrl);
  }
}
