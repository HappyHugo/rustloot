import { Component, Input, Output, OnChanges, SimpleChange, EventEmitter } from '@angular/core';
import { SimpleItem } from '../items'

@Component({
  selector: 'app-item',
  templateUrl: 'item.component.html',
  styleUrls: ['item.component.css']
})
export class ItemComponent implements OnChanges {
  @Input() item: SimpleItem;
  @Output() onSelect = new EventEmitter<SimpleItem>();
  photoUrl = 'https://steamcommunity-a.akamaihd.net/economy/image/'

  show = false;

  constructor() {}

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
  }

  select() {
    this.item.selected = !this.item.selected;
    this.onSelect.emit(this.item);
  }
}
