import { Injectable } from '@angular/core'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Items } from './items';

// RXJS
import { Observable } from 'rxjs/observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class DashboardService {
    // Just for dev
    url = 'http://api.steam.steamlytics.xyz/v1/inventory/76561198139868314/252490/2?key=1d752d3acbf1ff037c4f7255255d62ef&count=25'

    constructor(private http: Http) {

    }

    extractItems(res): JSON {
      const body = res.json();
      return body
    }

    getItems(): Observable<any> {
      console.log('Getting Itmes')
      return this.http.get(this.url).map(this.extractItems);
    }


}
