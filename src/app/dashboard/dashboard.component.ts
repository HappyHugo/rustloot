import { Component, OnInit, AfterContentInit, AfterViewChecked } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdDialog } from '@angular/material';
import { trigger, state, style, animate, transition } from '@angular/animations'



import { DashboardService } from './dashboard.service';
import { Items, SimpleItem } from './items';
import { TradeDialogComponent } from '../trade/trade.component'
import { ItemComponent } from './item/item.component'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [
    DashboardService,
  ],
  animations: [
    trigger('test', [
      state('closed', style({
        width: '0px'

      })),
      state('open', style({
        width: '256px'
      })),
      transition('closed => open', animate('100ms', style({
        width: '256px'
      }))),
      transition('open => closed', animate('100ms', style({
        width: '0px'
      })))
    ])
  ]
})
export class DashboardComponent implements OnInit, AfterContentInit {

  name = 'Hazard Semi Auto Rifle';

  photo = 'rtOnLXYSD-u65eusOk-nO4hCpUCJo2NbCxc2U4Y51MLNQ5Hz3URG1UJcBu0sv2Ko-M1Zj0mvYmKzVOblhEziZT2DqzUUnSAYyUNwwYoHB2nmqcnbGy22PrN4hQ'

  numberOfColumns: number;

  searchAnimation = 'closed';

  selectedItems: SimpleItem[] = [];

  testlist: SimpleItem[] = [
    {id: 1, name: this.name, photo: this.photo, selected: false},
    {id: 2, name: this.name, photo: this.photo, selected: false},
    {id: 3, name: this.name, photo: this.photo, selected: false},
    {id: 4, name: this.name, photo: this.photo, selected: false},
    {id: 5, name: this.name, photo: this.photo, selected: false},
    {id: 6, name: this.name, photo: this.photo, selected: false},
    {id: 7, name: this.name, photo: this.photo, selected: false},
    {id: 8, name: this.name, photo: this.photo, selected: false},
    {id: 9, name: this.name, photo: this.photo, selected: false},
    {id: 10, name: this.name, photo: this.photo, selected: false},
    {id: 11, name: this.name, photo: this.photo, selected: false},
    {id: 12, name: this.name, photo: this.photo, selected: false},
    {id: 13, name: this.name, photo: this.photo, selected: false},
    {id: 14, name: this.name, photo: this.photo, selected: false},
    {id: 15, name: this.name, photo: this.photo, selected: false},
    {id: 16, name: this.name, photo: this.photo, selected: false},
    {id: 17, name: this.name, photo: this.photo, selected: false},
    {id: 18, name: this.name, photo: this.photo, selected: false},
    {id: 19, name: this.name, photo: this.photo, selected: false},
    {id: 20, name: this.name, photo: this.photo, selected: false},
    {id: 21, name: this.name, photo: this.photo, selected: false},
    {id: 22, name: this.name, photo: this.photo, selected: false},
    {id: 23, name: this.name, photo: this.photo, selected: false},
    {id: 24, name: this.name, photo: this.photo, selected: false},
    {id: 25, name: this.name, photo: this.photo, selected: false},
    {id: 26, name: this.name, photo: this.photo, selected: false},
    {id: 27, name: this.name, photo: this.photo, selected: false},
    {id: 28, name: this.name, photo: this.photo, selected: false},
    {id: 29, name: this.name, photo: this.photo, selected: false},
    {id: 30, name: this.name, photo: this.photo, selected: false},
    {id: 31, name: this.name, photo: this.photo, selected: false},
    {id: 32, name: this.name, photo: this.photo, selected: false},
  ];

  constructor(
    private dashboardService: DashboardService,
    public dialog: MdDialog,
  ) {

  }

  animateSearchbar() {
    this.searchAnimation = (this.searchAnimation === 'closed' ? 'open' : 'closed')
    console.log('animate')
  }

  ngOnInit() {
   // this.getInventory()
   console.log('init')
  }

  ngAfterContentInit() {
    this.onResize();

    console.log(this.testlist)
    console.log(this.selectedItems)
  }

  onResize() {
    const elWidth = window.innerWidth * 0.4;
    const cols = (Math.floor(elWidth / 100))
    console.log('Columns: ' + cols + ' Width: ' + elWidth / 6)
    if ( cols === 0 ) {
      this.numberOfColumns = 1;
    } else {
      this.numberOfColumns = cols;
    }
  }

  findItem(id, array) {
    for (let i = 0; i <= array.length; i++ ) {
      if (array[i].id === id) {
        return i
      }
    }
    return -1;
  }

  onSelect(item: SimpleItem) {
    if (item.selected) {
      const index = this.findItem(item.id, this.testlist);
      if (index !== -1) {
        this.testlist.splice(index, 1);
        this.selectedItems.push(item)
      }
    } else {
      const index = this.findItem(item.id, this.selectedItems);
      if (index !== -1) {
        this.selectedItems.splice(index, 1);
        this.testlist.push(item)
      }
    }
  }

  clearSelected() {
    let done = 0;
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (this.selectedItems[i].selected === true) {
        this.selectedItems[i].selected = false;
        done++;
      }
    }
    if (done === this.selectedItems.length) {
      this.testlist = this.testlist.concat(this.selectedItems);
      this.selectedItems = [];
    } else {
      // error handling
    }
  }



  print(print: any) {
    console.log(print)
  }

  openTradeDialog() {
    this.dialog.open(TradeDialogComponent)
  }
}
