export interface SimpleItem {
  id: number,
  photo: string,
  name: string,
  selected: boolean,
}

export interface Items {
  assets: Asset[],
  descriptions: Description[],
  last_assetid: string,
  more_items: number,
  success: number,
  total_inventory_count: number,
}

interface Asset {
  amount: string,
  appid: string,
  assetid: string,
  classid: string,
  contextid: string,
  instanceid: string
}

interface Description {
  appid: number,
  background_color: string,
  classid: string,
  commodity: number,
  currency: number,
  descriptions: ItemDescription[],
  icon_url: string,
  icon_url_large: string,
  instanceid: string,
  market_hash_name: string,
  market_marketable_restriction: number,
  market_name: string,
  market_tradable_restriction: number,
  marketable: number,
  name: string,
  name_color: string,
  tags: Tag[],
  tradable: number,
  type: string
}

interface ItemDescription {
  type: string,
  value: string
}

interface Tag {
  category: string,
  internal_name: string,
  localized_category_name: string,
  localized_tag_name: string
}
