import { User } from './user';

export const USER: User = {
    id: '1',
    name: 'Ola Nordmann',
    photo: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_2_400x400.png'
}
