import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';

import { UserDialogComponent } from './user/dialog.component';
import { TradeDialogComponent } from './trade/trade.component'
import { ItemComponent } from './dashboard/item/item.component'
import { DashboardComponent } from './dashboard/dashboard.component';

import 'hammerjs'

const appRoutes: Routes = [
  { path: 'auth', redirectTo: '/auth' },

  { path: '', component: DashboardComponent},

  { path: '**', redirectTo: '/' },
];


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserDialogComponent,
    TradeDialogComponent,
    ItemComponent
  ],
  entryComponents: [
    UserDialogComponent,
    TradeDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    MaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
