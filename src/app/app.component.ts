import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MdIconRegistry, MdDialog } from '@angular/material';
import { User } from './user/user';
import { AppService } from './app.service'

// For when the server is not running
import { environment } from './../environments/environment';
import { USER } from './user/mock.user';
import { UserDialogComponent } from './user/dialog.component'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    AppService
  ]
})
export class AppComponent implements OnInit, AfterViewInit {

  user: User;

  height: any;

  constructor(
    iconRegistry: MdIconRegistry,
    sanitizer: DomSanitizer,
    public dialog: MdDialog,
    private appService: AppService,
  ) {
    iconRegistry.addSvgIcon('account',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_account_circle_black_24px.svg'));

    iconRegistry.addSvgIcon('logo',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/logo.svg'));

  }

  ngOnInit(): void {
    this.getLoggedInUser()
  }

  ngAfterViewInit() {
  }

  openUserDialog() {
    this.dialog.open(UserDialogComponent);
  }

  getLoggedInUser() {
    if (environment.production) {
        this.appService.getUser().subscribe(res => {
          /*
          console.log(this.getCookies().get('user'))
          console.log(this.getCookies().get('photo'))
          this.user.name = 'user';
          this.user.photo = 'photo';
          */
          this.user = res
        })
    } else {
      this.user = USER;
    }
  }

  logoutUser() {
    if (environment.production) {
      this.appService.closeSession().subscribe()
    } else {
      if (this.user.id === '1' && this.user.name === 'Ola Nordmann') {
        console.log('You are in debug')
      }
    }
  }

  getCookies(): Map<string , string> {
    const result = new Map();
    const cookies = document.cookie.split(/[=;]/);
    for (let i = 0; i < cookies.length; i += 2) {
      result.set(cookies[i].trim(), cookies[i + 1].trim());
    }

    console.log(cookies)

    return result;
  }



}

