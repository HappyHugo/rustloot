const https = require('https');
const fs = require('fs');
const express = require('express');
const session = require('express-session');
const path = require('path');
const passport = require('passport');
const RedisStore = require('connect-redis')(session);
const redis = require('redis');
const bodyparser = require('body-parser');
const RedisSession = require('redis-sessions');
const cookie = require('cookie');

const app = express();
const api = require('./api');
const steam = require('./openid')

//const bot = require('./bots/trade.js')'
const inventory = require('./inventory');

const serverOptions = {
    key: fs.readFileSync('./.certificates/server.key', 'utf8'),
    cert: fs.readFileSync('./.certificates/server.cert', 'utf8')
}

app.use(session({
  store: new RedisStore,
  secret: 'secret',
  saveUninitialized: true,
  resave: true,
  cookie: {
    secure: true,
    maxAge: 3600000
  },
  name: '_'
}));

rs = new RedisSession();

app.disable("x-powered-by");
app.use(express.static(path.join(__dirname, "/../dist")));
app.use(passport.initialize());
app.use(passport.session());
steam.openid(app, passport, rs);

api.user(app, cookie);

inventory.fetchInventory('76561197989769324');


const server = https.createServer(serverOptions, app)
server.on('listening', function(){
  console.log('Listening to ip ' + server.address().address + " at port " + server.address().port)
})
server.listen(8080)
