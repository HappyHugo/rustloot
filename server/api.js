module.exports = {
  user: function(app, cookie) {

    // GETS USER INFO
    app.get('/api/user/', isLoggedIn, function(req, res) {
      res.set("Content-Type", "application/json");
      res.send({
        id: req.user.id,
        name: req.user.name,
        photo: req.user.photo
      });
    });
  },
  // FUTURE ADMIN API
  admin: function() {
    console.log('Not yet implemented')
  },
}

// USER LOGGED IN MIDDLEWARE
const isLoggedIn = function(req, res, next) {
  if(req.user){
    return next();
  }
  res.redirect('/')
}
