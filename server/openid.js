const SteamStrategy = require('passport-steam');


module.exports = {
  openid: function(app, passport) {

    passport.serializeUser(function(user, done) {
      console.log('serializeUser: ' + user.id);
      // console.log(user);
      done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
      console.log('deserializeUser: ' + obj.id);
      done(null, obj)
    });

    passport.use(new SteamStrategy({
      returnURL: 'https://localhost:8080/auth/return',
      realm: 'https://localhost:8080/',
      apiKey: '555B15DBB5052FC8B48EDB51881D85D6'
    },
    (identifier, profile, done)=>{
      /**
       *  If we get a profile, see if it exists.
       *   - If it does, update it with new info.
       *   - If not, save it to the database.
       */

      if(profile){
        const user = {
          id: profile.id,
          name: profile.displayName,
          photo: profile.photos[0].value
        }
        done(null, user);
      }else{
        // What to do if we don't get a profile from openid.
      }
    }));

    app.get("/auth", passport.authenticate("steam", { failureRedirect: "/" }),
    function (req, res) {
      res.redirect("/");
    });

    app.get("/auth/return", passport.authenticate("steam", { failureRedirect: "/" }),
    function (req, res) {
      res.redirect('/')
    });

    app.get('/auth/logout', function(req, res){
      req.logout();
      res.redirect('/');
    });

  }

}
