import { RustlootWebsitePage } from './app.po';

describe('rustloot-website App', () => {
  let page: RustlootWebsitePage;

  beforeEach(() => {
    page = new RustlootWebsitePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
