import * as passport from "passport";
import { Strategy } from "passport-steam";

import { default as user, userModel } from "../models/user";
import { Request, Response, NextFunction} from "express";

passport.serializeUser(function(user: userModel, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  user.findById(id, (err, user)=>{
    if(err) return done(err);
    done(null, user)
  });
});


/**
 *    Sing in using OpenId and Steam
 */
passport.use(new Strategy({
    returnURL: 'http://localhost:1337/auth/return',
    realm: 'http://localhost:1337/',
    apiKey: '555B15DBB5052FC8B48EDB51881D85D6'
  },
  (identifier, profile, done)=>{
    /**
     *  If we get a profile, see if it exists.
     *   - If it does, update it with new info.
     *   - If not, save it to the database.
     */
  if(profile){
    const options = {
      upsert: true
    };
    let userInfo = {
      displayName: profile.displayName,
      photo: profile.photos[1].value
    }
    // Finds user and updates it, if it doesn't exists create a new one.
    user.findByIdAndUpdate(profile.id, userInfo, options, (err)=>{
      if(err) return console.log(err);
      done(null, profile)
    });
  }else{
    // What to do if we don't get a profile from openid.
  }
}));

export let auth = (req, res) =>{
  return passport.authenticate(req, res)
};

export let isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
  if (req.session.passport) {
    console.log("Du er logget inn!")
    return next()
  }else{
    console.log("Du er ikke logget inn")
  }
  res.redirect("/auth");
};

export let isAuthorized = (req: Request, res: Response, next: NextFunction) => {
  if (req.user) {
    return next()
  }
  res.redirect("/")
};

