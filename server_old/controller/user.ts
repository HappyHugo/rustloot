import { Request, Response } from "express";
import * as passport from "passport";
import { userModel } from "../models/user";

export let getSteamid = (req: Request & userModel, res: Response) => {
  res.set("Content-Type", "application/json");
  res.send({
    id: req.user._id,
    name: req.user.displayName,
    photo: req.user.photo,
  });
};
