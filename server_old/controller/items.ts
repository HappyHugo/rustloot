import { Request, Response } from "express";
import * as http from "http";
import { Observable } from "rxjs/observable";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

// http://api.steam.steamlytics.xyz/v1/inventory/{ STEAM_ID }/{ APP_ID }/{ CONTEXT_ID }?key=1d752d3acbf1ff037c4f7255255d62ef
const inventroyUrl = "http://api.steam.steamlytics.xyz/v1/inventory/";
const steamlyticsApiKey = "1d752d3acbf1ff037c4f7255255d62ef";

export let getInventroy = (req: Request, res: Response) => {
  const options = {
    host: "api.steam.steamlytics.xyz",
    path: "/v1/inventory/76561198139868314/252490/2?key=1d752d3acbf1ff037c4f7255255d62ef",
  };
  const request = http.get(options, (response) => {
    console.log("STATUS: " + response.statusCode);
    console.log("HEADERS: " + JSON.stringify(response.headers));

      // Buffer the body entirely for processing as a whole.
    const bodyChunks = [];
    response.on("data", (chunk) => {
      // You can process streamed parts here...
      bodyChunks.push(chunk);
    }).on("end", () => {
      const body = Buffer.concat(bodyChunks);
      console.log("BODY: " + body);
      // ...and/or process the entire body here.
    });

  });

  console.log("Hello");
  res.send({
    Hello: "Hello",
  });
};
