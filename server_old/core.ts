import * as bodyparser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as session from "express-session";
import * as passport from "passport";
import * as path from "path";
import * as q from "q";

// Authentication
import * as passportConfig from "./openid/passport";

// Controllers
import { getInventroy } from "./controller/items";
import { getSteamid } from "./controller/user";

// Models
import { userModel } from "./models/user";

/**
 *    Express setup
 */
const app: express.Application = express();

/**
 *    Connect to MongoDB.
 */
const mongoUri = "mongodb://localhost:27017/dev";
const mongoOptions = { promiseLibrary: require("q").Promise};

app.use(cookieParser());
app.disable("x-powered-by");
app.use(session({
  secret: "secret",
  resave: true,
  saveUninitialized: true,
}));

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
// Serves the website
app.use(express.static(path.join(__dirname, "/../client")));
///////////

/**
 *  Passport Routes
 */
app.get("/auth", passport.authenticate("steam", { failureRedirect: "/" }),
  (req, res) => {
    res.redirect("/");
  });

app.get("/auth/return", passport.authenticate("steam", { failureRedirect: "/" }),
  (req, res) => {
    res.redirect("/");
  });
///////////

/**
 *  API calls
 */
app.get("/api/user", passportConfig.isAuthorized, getSteamid);
app.get("/api/user/inventory", passportConfig.isAuthorized, getInventroy);
///////////

// catch 404 and forward to error handler
app.use((req: express.Request, res: express.Response, next) => {
  const err = new Error("Not Found");
  next(err);
});

// production error handler
// no stacktrace leaked to user
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.redirect("/");
});

export { app };
