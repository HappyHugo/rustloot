import * as http from "http";
import { app } from "./core";

const server = http.createServer(app);

server.on("listening", () => {
  console.log("Server is listening");
});

server.listen(1337);
