import * as mongoose from "mongoose";

export type userModel = mongoose.Document &{
  _id: string,
  displayName: string,
  photo: string,
}

const userSchema = new mongoose.Schema({
  _id: {type: String, required: true},
  displayName: String,
  photo: String
});

const user = mongoose.model("User", userSchema);
export default user;
