import { Router } from "express";

const loginRouter = Router();

loginRouter.get("/", (req, res)=>{
  res.json({title: "Rustloot"})
});

export {loginRouter};
